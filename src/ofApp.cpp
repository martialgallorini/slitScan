#include "ofApp.h"

// TODO :
// - check near and far clip
// - check trail fbo draw size : d�borde sur preview quand actif
// - flip image
// - move alpha mask
// - all draw into final comp FBO without using other FBOs
// - utiliser setAlphaMask pour appliquer le masque Alpha � la camera au lieu de la boucle for()


//--------------------------------------------------------------
void ofApp::setup(){
    
    ofBackground(0);
    ofSetFrameRate(60);
    kinect.init(false, true, true);
    kinect.open();
    
    scanFbo.allocate(worldWidth, worldHeight, GL_RGBA);
    scanFbo.begin();
    ofClear(0);
    scanFbo.end();
    
    trailFbo.allocate(worldWidth, worldHeight);
    trailFbo.begin();
    ofClear(0);
    trailFbo.end();
    
    compo.allocate(worldWidth, worldHeight, GL_RGBA);
    compo.begin();
    ofClear(0);
    compo.end();
    
    kinectRGB.allocate(640, 480, OF_IMAGE_COLOR_ALPHA);
    
    finalImage.allocate(640, 480, OF_IMAGE_COLOR_ALPHA);
    
    gui.setup();
    gui.setName("PARAMETERS");
    gui.add(bSyphon.set("publish syphon", false));
    gui.add(bTrail.set("background trail", false));
    gui.add(scanPosition.set("scan position", kinect.getWidth() / 2, 0, kinect.getWidth()));
    
    groupKinect.setName("KINECT");
    groupKinect.add(nearClip.set("near clip", 100, 0, 255));
    groupKinect.add(farClip.set("far clip", 200, 0, 255));
    groupKinect.add(dilatePass.set("dilate passes", 0, 1, 20));
    groupKinect.add(erodePass.set("erode passes", 0, 1, 20));
    groupKinect.add(blurPass.set("blur passes", 0, 1, 20));
    
    gui.add(groupKinect);
    
    gui.loadFromFile("settings.xml");
    
    //colNumber = 0;
    bSetup = true;
    
    alphaMask.allocate(kinect.getWidth(), kinect.getHeight());
    
    syphonServer.setName("slitScan");
    
    
}

//--------------------------------------------------------------
void ofApp::update(){
    
    kinect.update();
    
    if (kinect.isFrameNew()) {
        
        // create alpha mask from depth image
        ofPixels depthImage;
        depthImage = kinect.getDepthPixels();
        for (int y = 0; y < depthImage.getHeight(); y++) {
            for (int x = 0; x < depthImage.getWidth(); x++) {
                ofColor c = depthImage.getColor(x, y);
                if (c.getBrightness() > nearClip && c.getBrightness() < farClip) {
                    alphaMask.getPixels().setColor(x, y, ofColor(255, 255));
                }
                else {
                    alphaMask.getPixels().setColor(x, y, ofColor(0, 255));
                }
            }
        }
        alphaMask.flagImageChanged();
        
        // filter alpha for cleaner mask
        if (dilatePass > 0) {
            for (int i = 0; i < dilatePass; i++) {
                alphaMask.dilate();
            }
        }
        if (erodePass > 0) {
            for (int i = 0; i < erodePass; i++) {
                alphaMask.erode();
            }
        }
        if (blurPass > 0) {
            for (int i = 0; i < blurPass; i++) {
                alphaMask.blurGaussian();
            }
        }
        
        // combine RGB image with alpha mask
        kinectRGB.setFromPixels(kinect.getPixels());
 
        //kinectRGB.getTexture().setAlphaMask(alphaMask.getTexture());
        
        for (int y = 0; y < kinectRGB.getHeight(); y++) {
            for (int x = 0; x < kinectRGB.getWidth(); x++) {
                ofColor newColor(kinectRGB.getColor(x, y), alphaMask.getPixels().getColor(x, y).getBrightness());
                finalImage.setColor(x, y, newColor);
            }
        }
        finalImage.update();
        
        //finalImage = kinectRGB;
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    if(kinect.isFrameNew()) {
        
        // draw slitscan effect in FBO with alpha
        
        scanFbo.begin();
        finalImage.drawSubsection(0, (scanFbo.getHeight() - finalImage.getHeight()) / 2, scanPosition, finalImage.getHeight(), 0, 0);
        //finalImage.draw(0, , finalImage.getWidth(), finalImage.getHeight());
        scanFbo.getTexture().drawSubsection(scanPosition + 1, 0, worldWidth - scanPosition - 1, worldHeight, scanPosition, 0);
        finalImage.getTexture().drawSubsection(scanPosition, (scanFbo.getHeight() - finalImage.getHeight()) / 2, 1, finalImage.getHeight(), scanPosition, 0);
        scanFbo.end();
        
        // draw trail effect in FBO
        trailFbo.begin();
        for(int i = 0; i < scanFbo.getWidth(); i++) {
            kinect.getTexture().drawSubsection(i, 0, 1, worldHeight, scanPosition, 0);
        }
        trailFbo.end();
        
        // combine FBOs into one FBO to publish to syphon
        compo.begin();
        // draw trail effect in background
        if(bTrail) {
            trailFbo.draw(0, 0);
        }
        // draw slitscan effect in front
        scanFbo.draw(0, 0);
        compo.end();
        
//        colNumber++;
//        if (colNumber > scanFbo.getWidth()) {
//            colNumber = 0;
//        }
        
        // draw kinect RGB image with scan reference mark
        ofPushMatrix();
        ofTranslate(0 , 0);
        finalImage.draw(0, 0, 320, 240);
        int lineX = ofMap(scanPosition, 0, kinect.getWidth(), 0, 320);
        ofSetColor(0, 255, 0);
        ofDrawLine(lineX - 1, 0, lineX + 1, 240);
        ofSetColor(255);
        ofPopMatrix();
        
        // draw kinect depth image
        kinect.drawDepth(560, 0, 320, 240);
        
        // draw alpha mask
        alphaMask.draw(880, 0, 320, 240);
 
        // draw final composition preview in window
        compo.draw(0, 250, ofGetWidth(), ofGetHeight() - 250);
        
        if (bSyphon) {
            syphonServer.publishTexture(&compo.getTexture());
        }
        
    }
    
    if(bSetup) {
        gui.setPosition(340, 0);
        gui.draw();
    }
}

//--------------------------------------------------------------
void ofApp::exit(){
    kinect.close();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){
    
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){
    
}
