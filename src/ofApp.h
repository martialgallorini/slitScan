#pragma once

#include "ofMain.h"
#include <ofxGui.h>
#include <ofxKinect.h>
#include <ofxCvGrayscaleImage.h>
#include "ofxSyphon.h"

const unsigned int worldWidth = 1280;
const unsigned int worldHeight = 800;

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    void exit();
		
    ofVideoGrabber video;
    
    ofFbo scanFbo;
    ofFbo trailFbo;
    ofFbo compo;
    
    //int colNumber;
    
    ofImage scanLine;
    
    ofxPanel gui;
    ofParameter<bool> bSyphon;
    ofParameter<bool> bTrail;
    ofParameter<int> scanPosition;
    
    ofParameterGroup groupKinect;
    ofParameter<float> nearClip;
    ofParameter<float> farClip;
    ofParameter<int> blurPass;
    ofParameter<int> erodePass;
    ofParameter<int> dilatePass;

    
    bool bSetup;

    ofxKinect kinect;
        
    ofxCvGrayscaleImage alphaMask;
    ofImage kinectRGB;
    ofImage finalImage;
    
    ofxSyphonServer syphonServer;
};
